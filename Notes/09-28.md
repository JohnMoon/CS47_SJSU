Memory Model
===================
September 28, 2015

### Data Alignment in Memory ###
```Assembly
# exp11.asm
.include "cs47_macro.asm"

.data
.align 0 # This parameter forces the memory to all stay in one block
var_a: .byte 0x10
var_b: .half 0x3210
var_c: .byte 0x20
var_d: .word 0x76543210

.text
.globl main
main:
	# Variables are read into reg from mem
	lb $s0, var_a # R[s0] = M[var_a] (7:0)
	lh $s1, var_b # R[s1] = M[var_b] (15:0)
	lb $s2, var_c # R[s2] = M[var_c] (7:0)
	lw $s3, var_d # R[s3] = M[var_d] (31:0)

# System exit
exit
```
So what's going on in this code snippet? Look in his slides to see more about what's going on with how the memory is aligned specifically (needs charts and stuff). Basically, if we don't specify that the memory should be aligned bytewise, halfwise, wordwise, etc, there won't be any specific alignment. Your align condition can come in between memory assignments too.

There are ways to order your variable declarations to make your memory aligned. This can end up saving space on your memory footprint, especially if it's in an object that's used millions of times!

#### Half Word Alignment in Memory ####
Typically, we don't want half words to cross a memory address (i.e. some of it is stored at one address and the rest is stored on the next level.

`[x][x][ ][ ] is cool!`

`[ ][ ][x][x] is cool!`

`[ ][x][x][ ] is not very cool.`

`
[ ][ ][ ][x]
[x][ ][ ][ ] is not very cool.
`

Simply, for half word storage, we just drop supporting cofigurations where half word storage can occur on odd addresses.

#### Word Alignment in Memory ####
The only way words can be aligned is all on one level (i.e. `[x][x][x][x]`). All other configurations would have the words overlapping memory addresses and would cause system complexity and unnecessary extra data access time.

### Stack Alignment ###
- Stack is word accessible space
- It starts from 0x7ffffffc to be aligned for word storage
- Stack needs Push and Pop operation
	- MIPS does not provide native push and pop operation
	- It is done by manipulating `$sp` directly
- Push operation should store data into address pointed by `$sp` and decrease `$sp` by 4 (since it's a word)
- Pop operation should do the opposite. First, increase `$sp` by 4 and then read word data from that location

#### Stack Operations ####
```Assembly
# Push
sw $s0, 0x0($sp)
addi $sp, $sp, -4
sw $s1, 0x0($sp)
addi $sp, $sp, -4
sw $s2, 0x0($sp)
addi $sp, $sp, -4
sw $s3, 0x0($sp)
addi $sp, $sp, -4

# Pop
addi $sp, $sp, +4
lw $t0, 0x0($sp)
addi $sp, $sp, +4
lw $t1, 0x0($sp)
addi $sp, $sp, +4
lw $t2, 0x0($sp)
addi $sp, $sp, +4
lw $t3, 0x0($sp)
```
As homework, add to our macros file by creating push and pop operations!
