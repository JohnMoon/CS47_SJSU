###############################################################################
#
# File: selection_sort.asm
#
# Author: John Moon <johncarlmoon@gmail.com>
#
# Purpose: Write a program which defines total m integer numbers in pre-defined
# data area 'var_a'. The number m is stored in a location var_m. The program
# implements a selection sort on the array variable var_a.
#
###############################################################################

### Macros ###

.macro print_int($arg)
	li	$v0, 1		 # System call code for print_int
	li	$a0, $arg	 # Integer to print
	syscall			 # Print the integer
.end_macro

.macro print_str($arg)
	li	$v0, 4		 # System call code for print_str
	la	$a0, $arg	 # Address of the string to print
	syscall			 # Print the string	
.end_macro

.macro print_reg_int ($arg)
	li $v0, 1 		# print_int call
	move $a0, $arg 		# move the source reg value to $a0
	syscall
.end_macro

### Data Segment ###
.data
before: .asciiz "Before sorting: "
after: .asciiz "After sorting: "
sp: .asciiz " "
nl: .asciiz "\n"

### Test Data ###
.align 2
var_a: .word 4 8 3 0 1 5 2 7 3 6
var_m: .word 10

### Code Segment ###
.text
main:
	la $s0, var_a
	li $s1, 0	# $s1 = i
	lw $s2, var_m	# $s2 = var_a.length()

	# Print the pre-sorted array #
	la $t2, var_a
	li $t1, 0
	print_str(before)
	jal printVarA
	print_str(nl)

# Controls low index of unsorted part of the list #
loop1:
	slt $t9, $s1, $s2	# if (i < var_a.length())
	beqz $t9, exit		# Exit if list is done
	li $s3, 0		# $s3 = j
	sub $s4, $s2, $s1	# Unsorted length = arr.length() - i
	sub $s4, $s4, 1
	la $t1, ($s0)		# Copies first (potentially smallest) addr - static pointer
	la $t2, 4($s0)		# Same copy, this one is a traversal pointer
	li $t5, 0		# Boolean, a flip doesn't need to occur yet
	jal loop2
	addi $s1, $s1, 1	# Increment loop index i
	addi $s0, $s0, 4	# Move the array up one element
	j loop1

# Finds the smallest unsorted value, stores at addr $t1. #
loop2:
	slt $t9, $s3, $s4	# if (j < unsorted.length())
	beqz $t9, flip		# Done with search, flip the elements
	lw $t3, 0($t1)		# Loads value from static pointer
	lw $t4, 0($t2)		# Loads value from traversal pointer
	sle $t9, $t3, $t4	# if (static <= traversal)
	beq $t9, 1, inc		# nothing to do, jump to increment step
	li $t5, 1		# Boolean, a flip needs to occur
	la $t1, ($t2)		# else, static is new smallest
inc:
	add $t2, $t2, 4		# Increment traversal ponter
	add $s3, $s3, 1		# Increment loop index j
	j loop2

# Moves smallest value [0($t1)] to the leftmost unsorted spot [0($s0)]
flip:
	beqz $t5, jumpRA	# If a flip needn't occur, don't flip
	lw $t3, 0($t1)
	lw $t4, 0($s0)
	sw $t3, 0($s0)
	sw $t4, 0($t1)
	j jumpRA

jumpRA:
	jr $ra

# Prints the array stored in var_a #
printVarA:
	slt $t9, $t1, $s2
	beqz $t9, jumpRA
	lw $t3, 0($t2)
	print_reg_int($t3)
	print_str(sp)
	add $t1, $t1, 1
	add $t2, $t2, 4
	j printVarA

exit:
	# Prints the post-sorted array #
	la $t2, var_a
	li $t1, 0
	print_str(after)
	jal printVarA
	print_str(nl)
	li $v0, 10
	syscall
