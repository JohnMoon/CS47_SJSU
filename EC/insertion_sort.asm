###############################################################################
#
# File: insertion_sort.asm
#
# Author: John Moon <johncarlmoon@gmail.com>
#
# Purpose: Write a program which defines total m integer numbers in before-defined
# data area 'var_a'. The number m is stored in a location var_m. The program
# implements an insertion sort on the array variable var_a.
#
###############################################################################

### Macros ###

.macro print_int($arg)
	li	$v0, 1		 # System call code for print_int
	li	$a0, $arg	 # Integer to print
	syscall			 # Print the integer
.end_macro

.macro print_str($arg)
	li	$v0, 4		 # System call code for print_str
	la	$a0, $arg	 # Address of the string to print
	syscall			 # Print the string	
.end_macro

.macro print_reg_int ($arg)
	li $v0, 1 		# print_int call
	move $a0, $arg 		# move the source reg value to $a0
	syscall
.end_macro

### Data Segment ###
.data
before: .asciiz "Before sorting: "
after: .asciiz "After sorting: "
sp: .asciiz " "
nl: .asciiz "\n"

### Test Data ###
.align 2
var_a: .word 4 8 3 0 1 5 2 7 3 6
var_m: .word 10

.text
main:
	# Print the array pre-sorted array #
	la $s0, var_a
	li $s1, 0
	lw $s2, var_m
	print_str(before)
	jal printVarA
	print_str(nl)
	
	li $t0, 1	# $t0 = i = 1
	li $t3, 0	# $t3 = j = 0
	lw $t1, var_m	# $t1 = var_a.length()
	sub $t1, $t1, 1
	la $t2, var_a
	
loop1:
	bgt $t0, $t1, exit	# while i < var_a.length()
	sub $t3, $t0, 1		# i - 1
	mul $t4, $t0, 4		# Byte shifting to the ith element
	add $t4, $t2, $t4	# Shift
	lw $t5, 0($t4)		# Loading the particular number (n)

loop2:
	blt $t3, 0, breakL2	# while j >= 0
	mul $t4, $t3, 4		# Byte shifting to the jth element
	add $t4, $t2, $t4	# Shift
	lw $t6, 0($t4)		# Loading the particular number (m)
	ble $t6, $t5, breakL2	# if var_a[j] <= var_a[i], exit
	sw $t6, 4($t4)		# else, m = var_a[j + 1]
	sub $t3, $t3, 1		# j--
	j loop2
	
breakL2:
	mul $t4, $t3, 4		# Byte shifting to jth element
	add $t4, $t2, $t4	# Shift
	sw $t5, 4($t4)		# Storing var_a[i] in var_a[j + 1]
	addi $t0, $t0, 1	# i++
	j loop1

jumpRA:
	jr $ra

# Prints the array stored in var_a #
printVarA:
        slt $t9, $s1, $s2
        beqz $t9, jumpRA
        lw $t3, 0($s0)
        print_reg_int($t3)
        print_str(sp)
        add $s1, $s1, 1
        add $s0, $s0, 4
        j printVarA

exit:
	# Prints the post-sorted array #
	la $s0, var_a
        li $s1, 0
	print_str(after)
	jal printVarA
	print_str(nl)
	li $v0, 10
	syscall
