.data
enterhi: .asciiz "Enter a number for Hi ? "
enterlo: .asciiz "Enter a number for Lo ? "
preswap: .asciiz "Before swapping "
postswap: .asciiz "After swapping "
newline: .asciiz "\n"
stringHi: .asciiz "Hi "
stringLo: .asciiz "Lo "
stringComma: .asciiz ", "
stringEqual: .asciiz "= "

.macro print_Hi_Lo($strHi, $strComma, $strLo, $strEqual)
	print_str($strHi)
	print_str($strEqual)

	mfhi $t0
	print_reg_int($t0)

	print_str($strComma)
	print_str($strLo)
	print_str($strEqual)

	mflo $t1
	print_reg_int($t1)

	print_str(newline)
.end_macro

.macro print_str($arg)
	li $v0, 4
	la $a0, $arg
	syscall
.end_macro

.macro print_reg_int($arg)
	li $v0, 1
	move $a0, $arg
	syscall
.end_macro

.macro swap_Hi_Lo()
	mfhi $t1
	mflo $t2
	mthi $t2
	mtlo $t1
.end_macro

.text
.globl main
main: 
	print_str(enterhi)
	li $v0, 5
	syscall
	move $t2, $v0
	mthi $t2
	
	print_str(enterlo)
	li $v0, 5
	syscall
	move $t3, $v0
	mtlo $t3

	print_str(preswap)
	print_Hi_Lo(stringHi, stringComma, stringLo, stringEqual)
	swap_Hi_Lo
	print_str(postswap)
	print_Hi_Lo(stringHi, stringComma, stringLo, stringEqual)
	
	li $v0, 10
	syscall

