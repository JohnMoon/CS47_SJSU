.macro print_str($arg)
	li $v0, 4
	la $a0, $arg
	syscall
.end_macro

.macro print_reg_int($arg)
	li $v0, 1
	move $a0, $arg
	syscall
.end_macro

.data
addingA: .asciiz "Moving from array (a) to resultant array: "
addingB: .asciiz "Moving from array (b) to resultant array: "
newline: .asciiz "\n"
.align 2
#------ Test 1 ------#
var_a: .word 5 6 7 8 14 15 17
var_b: .word 2 3 5 5 8 10 11 17 18 20
var_m: .word 7
var_n: .word 10

## Expected sequence in this location - 2 3 5 5 5 6 7 8 8 10 11 14 15 17 17 18 20
var_c: .word 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0

.text
main:
	la $t1, var_a
	la $t2, var_b
	la $t3, var_c
	lw $t4, var_m	  # var_a length
	lw $t5, var_n	  # var_b length
	add $s0, $t4, $t5 # Combined length
	li $s1, 0	  # Index for A
	li $s2, 0	  # Index for B
	li $s3, 0	  # Combined index
	sge $t7, $t4, $t5
	beq $t7, 1, loopA
	beq $t7, 0, loopB

loopA:
	slt $t7, $s3, $s0
	beq $t7, 0, exit
	lw $t8, 0($t1)
	lw $t9, 0($t2)
	la $ra, loopA
	slt $t7, $s2, $t5
	beq $t7, 0, AtoC
	sle $t7, $t8, $t9
	beq $t7, 1, AtoC
	beq $t7, 0, BtoC

loopB:
	slt $t7, $s3, $s0
	beq $t7, 0, exit
	lw $t8, 0($t1)
	lw $t9, 0($t2)
	la $ra, loopB
	slt $t7, $s1, $t4
	beq $t7, 0, BtoC
	sle $t7, $t8, $t9
	beq $t7, 1, AtoC
	beq $t7, 0, BtoC

AtoC:
	lw $t9, 0($t1)
	sw $t9, 0($t3)
	print_str(addingA)
	print_reg_int($t9)
	print_str(newline)
	addi $t1, $t1, 4
	addi $t3, $t3, 4
	addi $s1, $s1, 1
	addi $s3, $s3, 1
	jr $ra
	
BtoC:
	lw $t9, 0($t2)
	sw $t9, 0($t3)
	print_str(addingB)
	print_reg_int($t9)
	print_str(newline)
	addi $t2, $t2, 4
	addi $t3, $t3, 4
	addi $s2, $s2, 1
	addi $s3, $s3, 1
	jr $ra

exit:
	li $v0, 10
	syscall
