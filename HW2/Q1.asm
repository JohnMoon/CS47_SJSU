.data
enterN: .asciiz "How many Fibbonacci numbers should be generated? "
pushing: .asciiz "Pushing onto stack: "
newline: .asciiz "\n"

.macro print_str($arg)
	li $v0, 4
	la $a0, $arg
	syscall
.end_macro

.macro print_reg_int($arg)
	li $v0, 1
	move $a0, $arg
	syscall
.end_macro

.macro push($data)
	print_str(pushing)
	print_reg_int($data)
	print_str(newline)
	addi $sp, $sp, -4  	# Decrement stack pointer by 4
	sw	 $data, 0($sp)	# Save data to stack
.end_macro

.text
main:
	# Get the number of sequence elements to generate (N) #
	print_str(enterN)
	li $v0, 5
	syscall
	move $s0, $v0

	# Push 0 and 1 onto stack if N > 0 and 1 respectively #
	sgt $t2, $s0, $zero
	beq $t2, $zero, exit
	push ($zero)

	sgt $t2, $s0, 1
	beq $t2, $zero, exit
	li $t1, 1
	push ($t1)

	# Loop to push rest of sequence onto stack if N > 2 #
	sgt $t2, $s0, 2
	beq $t2, $zero, exit
	li $t3, 0 		# Index controlling the loop (I)

loop:
	sub $t1, $s0, 2
	slt $t2, $t3, $t1	# If I < N - 2
	beq $t2, $zero, exit

	lw $t4, 0($sp)
	lw $t5, 4($sp)
	add $t1, $t4, $t5
	push ($t1)		# push(stack[0] + stack[-1])
	add $t3, $t3, 1		# Increment I
	j loop

exit:
	li $v0, 10
	syscall