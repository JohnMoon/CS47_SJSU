# exp11.asm
.include "cs47_macro.asm"

.data
.align 1
var_a: .byte 0x10
var_c: .half 0x3210
var_b: .byte 0x20
var_d: .word 0x76543210

.text
.globl main
main:
	# Variables are read into reg from mem
	lb $s0, var_a # R[s0] = M[var_a] (7:0)
	lh $s1, var_b # R[s1] = M[var_b] (15:0)
	lb $s2, var_c # R[s2] = M[var_c] (7:0)
	lw $s3, var_d # R[s3] = M[var_d] (31:0)

# System exit
exit
