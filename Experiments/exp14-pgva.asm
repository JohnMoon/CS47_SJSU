.include "cs47_macro.asm"

.data
msg1:	.asciiz "Global var value is "
strCR:	.asciiz "\n"
msg2:	.asciiz "	---> Control at print_gva\n"

.text
.globl print_gva
print_gva:
	print_str(msg2)
	print_str(msg1)
	lw $t1, glob_var_a
	print_reg_int($t1)
	print_str(strCR)
	jr $ra
