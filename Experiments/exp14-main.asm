.include "cs47_macro.asm"

.extern glob_var_a 4

.data 
msg1:	.asciiz "Enter and integer: "
msg2:	.asciiz "---> Control back to main\n"

.text
.globl main
main:
	print_str(msg1)
	read_int($t0)
	sw $t0, glob_var_a
	jal print_gva
L1: print_str(msg2)

exit
Q