.include "./cs47_proj_macro.asm"

.data
div_zero_error: .asciiz "error - division by zero\n"
.text
#-----------------------------------------------
# C style signature 'printf(<format string>,<arg1>,
#			 <arg2>, ... , <argn>)'
#
# This routine supports %s and %d only
#
# Argument: $a0, address to the format string
#	    All other addresses / values goes into stack
#-----------------------------------------------
printf:
	#store RTE - 5 *4 = 20 bytes
	addi	$sp, $sp, -24
	sw	$fp, 24($sp)
	sw	$ra, 20($sp)
	sw	$a0, 16($sp)
	sw	$s0, 12($sp)
	sw	$s1,  8($sp)
	addi	$fp, $sp, 24
	# body
	move 	$s0, $a0 #save the argument
	add     $s1, $zero, $zero # store argument index
printf_loop:
	lbu	$a0, 0($s0)
	beqz	$a0, printf_ret
	beq     $a0, '%', printf_format
	# print the character
	li	$v0, 11
	syscall
	j 	printf_last
printf_format:
	addi	$s1, $s1, 1 # increase argument index
	mul	$t0, $s1, 4
	add	$t0, $t0, $fp # all print type assumes
			      # the latest argument pointer at $t0
	addi	$s0, $s0, 1
	lbu	$a0, 0($s0)
	beq 	$a0, 'd', printf_int
	beq	$a0, 's', printf_str
	beq	$a0, 'c', printf_char
printf_int:
	lw	$a0, 0($t0) # printf_int
	li	$v0, 1
	syscall
	j 	printf_last
printf_str:
	lw	$a0, 0($t0) # printf_str
	li	$v0, 4
	syscall
	j 	printf_last
printf_char:
	lbu	$a0, 0($t0)
	li	$v0, 11
	syscall
	j 	printf_last
printf_last:
	addi	$s0, $s0, 1 # move to next character
	j	printf_loop
printf_ret:
	#restore RTE
	lw	$fp, 24($sp)
	lw	$ra, 20($sp)
	lw	$a0, 16($sp)
	lw	$s0, 12($sp)
	lw	$s1,  8($sp)
	addi	$sp, $sp, 24
	jr $ra
	
#####################################################################
# twos_complement
#
# Argument:
#	$a0: Number to be complemented
#
# Return:
#	$v0: Two's complement of the argument
#####################################################################
twos_complement:
	move $s0, $a0
	not $s0, $s0
	li $s1, 1
	move $a0, $s0
	move $a1, $s1
	move $a3, $ra
	jal add_logic
	jr $a3
	
#####################################################################
# bit_replicator
#
# Argument:
#	$a0: Number to be bit replicated
#
# Return:
#	$v0: Bit replicated argument
#####################################################################
bit_replicator:
	beqz $a0, zeros
ones:
	li $v0, 0xFFFFFFFF
	jr $ra
zeros:
	li $v0, 0x00000000
	jr $ra
	
#####################################################################
# au_logical 
#
# Argument:
# 	$a0: First number
#	$a1: Second number
#	$a2: operation code ('+':add, '-':sub, '*':mul, '/':div)
#
# Return:
#	$v0: ($a0+$a1) | ($a0-$a1) | ($a0*$a1):LO | ($a0 / $a1)
# 	$v1: ($a0 * $a1):HI | ($a0 % $a1)
#####################################################################
au_logical:
	beq $a2, '+', add_logic
	beq $a2, '-', sub_logic
	beq $a2, '*', mul_logic
	beq $a2, '/', div_logic
	jr $ra
	
#####################################################################
# add_logic
#
# Argument:
# 	$a0: First number
#	$a1: Second number
#
# Return:
#	$v0: ($a0+$a1)
#####################################################################
add_logic:
	li $a2, 0
	j adder_subber
	
#####################################################################
# sub_logic
#
# Argument:
# 	$a0: First number
#	$a1: Second number
#
# Return:
#	$v0: ($a0-$a1)
#####################################################################
sub_logic:
	li $a2, 1
	j adder_subber

#####################################################################
# adder_subber
#
# Argument:
# 	$a0: First number
#	$a1: Second number
#	$a2: SnA (0 = add, 1 = sub)
#
# Return:
#	$v0: ($a0 +/- $a1)
#####################################################################
adder_subber:
	move $s7, $a0
	move $s6, $a1
	move $s5, $a2
	li $s0, 0			# i  = $s0 = 0
	li $s1, 0			# s = $s1 = 0
	li $s2, 0
	beqz $s5, addition_loop
	not $s6, $s6			# Subtraction, so ~$a1
	addi $s6, $s6, 1
	
addition_loop:
	extract_nth_bit($s7, $s0, $s4)
	extract_nth_bit($s6, $s0, $s5)
	
	# Calculating result #
	xor $t6, $s4, $s5
	xor $t7, $t6, $s2
	insert_to_nth_bit($s1, $s0, $t7)
	
	# Calculating carry out #
	and $t7, $t6, $s2
	and $t8, $s4, $s5
	or $s2, $t7, $t8
	
	# Incrementing #
	add $s0, $s0, 1
	blt $s0, 32, addition_loop
	
	move $v0, $s1
	jr $ra

#####################################################################
# mul_logic
#
# Argument:
# 	$a0: First number
#	$a1: Second number
#
# Return:
#	$v0: ($a0 * $a1)
#	$v1: ($a0 * $a1)
#
# Notes: $v0 result is the lo reg, $v1 is the hi reg
#####################################################################
mul_logic:
	li $t0, 0	# $t0 = i = 0
	li $t1, 0	# $t1 = h = 0
	move $t2, $a0	# $t2 = l = MPLR
	move $t3, $a1	# $t3 = m = MCND
	
mul_logic_loop:
	extract_nth_bit($t2, $0, $a0)	# r = {32{l[0]}}
	move $t4, $ra
	jal bit_replicator
	move $ra, $t4
	and $t4, $v0, $t3 	# x = m AND r
	move $a0, $t1
	move $a1, $t4
	li $a2, '+'
	move $t1, $ra
	jal au_logical
	move $ra, $t1
	move $t1, $v0		# h = h + x
	srl $t2, $t2, 1		# l = l >> 1
	extract_nth_bit($t1, $0, $t4)
	li $t5, 31
	insert_to_nth_bit($t2, $t5, $t4)
	srl $t1, $t1, 1
	addi $t0, $t0, 1
	blt $t0, 32, mul_logic_loop 
	move $v0, $t2
	jr $ra

#####################################################################
# div_logic
#
# Argument:
# 	$a0: First number
#	$a1: Second number
#
# Return:
#	$v0: ($a0 / $a1)
#	$v1: ($a0 % $a1)
#
# Notes: $v0 result is the lo reg, $v1 is the hi reg
#####################################################################
div_logic:
	beqz $a1, div_zero
	slti $t0, $a0, 0 
	slti $t1, $a1, 0
	xor $s3, $t0, $t1  # if $s3 = 1, then final answer is negative
	abs $a0, $a0
	abs $a1, $a1

div_unsigned:
	li $t0, 0	# $t0 = i = 0
	move $t1, $a0	# $t1 = q = DVND
	move $t2, $a1	# $t2 = d = DVSR
	li $t3, 0	# $t3 = r = 0

div_logic_loop:
	sll $t3, $t3, 1
	li $t4, 31
	extract_nth_bit($t1, $t4, $t5)
	insert_to_nth_bit($t3, $0, $t5)
	sll $t1, $t1, 1
	move $a0, $t3
	move $a1, $t2
	li $a2, '-'
	move $t4, $ra
	jal au_logical
	move $ra, $t4
	move $t4, $v0	# $t4 = s = r - d
	bltz $t4, div_increment
	move $t3, $t4
	li $t4, 1
	insert_to_nth_bit($t1, $0, $t4)

div_increment:
	addi $t0, $t0, 1
	blt $t0, 32, div_logic_loop
	beqz $s3, div_answer
	move $a0, $t1
	move $t2, $ra
	jal twos_complement
	move $t1, $v0
	move $a0, $t3
	jal twos_complement
	move $t3, $v0
	move $ra, $t2

div_answer:
	move $v0, $t1
	move $v1, $t3
	jr $ra

#####################################################################
# au_normal
#
# Argument:
# 	$a0: First number
#	$a1: Second number
#	$a2: operation code ('+':add, '-':sub, '*':mul, '/':div)
#
# Return:
#	$v0: ($a0+$a1) | ($a0-$a1) | ($a0*$a1):LO | ($a0 / $a1)
# 	$v1: ($a0 * $a1):HI | ($a0 % $a1)
#####################################################################
au_normal:
	beq $a2, '+', add_normal
	beq $a2, '-', sub_normal
	beq $a2, '*', mul_normal
	beq $a2, '/', div_normal
	jr $ra

#####################################################################
# add_normal
#
# Argument:
# 	$a0: First number
#	$a1: Second number
#
# Return:
#	$v0: ($a0+$a1)
#####################################################################
add_normal:
	move $t0, $a0		# x = $t0
	move $t1, $a1		# y = $t1
	add $t2, $t0, $t1	# x + y = $t0 + $t1
	move $v0, $t2		# Copy answer to $v0 #
	jr $ra

#####################################################################
# sub_normal
#
# Argument:
# 	$a0: First number
#	$a1: Second number
#
# Return:
#	$v0: ($a0-$a1)
#####################################################################
sub_normal:
	move $t0, $a0		# x = $t0
	move $t1, $a1		# y = $t1
	sub $t2, $t0, $t1	# x - y = $t0 - $t1
	move $v0, $t2		# Copy answer to $v0 #
	jr $ra
	
#####################################################################
# mul_normal
#
# Argument:
# 	$a0: First number
#	$a1: Second number
#
# Return:
#	$v0: ($a0 * $a1)
#	$v1: ($a0 * $a1)
#
# Notes: $v0 result is the lo reg, $v1 is the hi reg
#####################################################################
mul_normal:
	move $t0, $a0	# x = $t0
	move $t1, $a1	# y = $t1
	li $t3, 0	# xy_1 = $t3
	li $t4, 0	# xy_2 = $t4
	li $t5, 0	# i
	bltz $t1, mul_normal_loop_neg
	
mul_normal_loop_pos:
	bge $t5, $t1, exit_mul_normal_pos
	addu $t3, $t3, $t0
	addi $t5, $t5, 1
	j mul_normal_loop_pos
	
mul_normal_loop_neg:
	ble $t5, $t1, exit_mul_normal_neg
	add $t3, $t3, $t0
	subi $t5, $t5, 1
	j mul_normal_loop_neg
	
exit_mul_normal_pos:
	move $v0, $t3 # Copy answer to $v0 #
	jr $ra

exit_mul_normal_neg:
	not $t3, $t3
	addi $t3, $t3, 1
	move $v0, $t3
	jr $ra

#####################################################################
# div_normal
#
# Argument:
# 	$a0: First number
#	$a1: Second number
#
# Return:
#	$v0: ($a0 / $a1)
#	$v1: ($a0 % $a1)
#
# Notes: $v0 result is the lo reg, $v1 is the hi reg
#####################################################################
div_normal:
	beqz $t1, div_zero
	slti $t0, $a0, 0
	slti $t1, $a1, 0
	xor $s3, $t0, $t1  # if $s3 = 1, then final answer is negative
	abs $a0, $a0
	abs $a1, $a1
	
	move $t0, $a0	# x = $t0
	move $t1, $a1	# y = $t1
	li $t3, 0	# x/y = $t3
	li $t5, 0	# n = $t5

# How many times does $t1 go into $t0? #
div_normal_loop:
	bge $t5, $t0, exit_div_normal
	add $t5, $t5, $t1
	add $t3, $t3, 1
	j div_normal_loop
	
exit_div_normal:
	beq $t5, $t0, results_good
	sub $t5, $t5, $t1
	sub $t3, $t3, 1
	sub $t6, $t0, $t5
	
	beqz $s3, results_good
	move $a0, $t3
	move $t2, $ra
	jal twos_complement
	move $t3, $v0
	move $a0, $t6
	jal twos_complement
	move $t6, $v0
	move $ra, $t2
	
results_good:
	move $v0, $t3	# Copy quotient to $v0 #
	move $v1, $t6	# Copy remainder to $v1 #
	jr $ra
	
div_zero:
	print_str(div_zero_error)
	exit