###############################################################################
#
# File: proj-main.asm
#
# Author: John Moon <johncarlmoon@gmail.com>
#
# Purpose: This is the main testing program for my arithmetic functions defined
# in the procedures file. It will test all the arithmetic operations in both
# normal and logical flavors and compare the results. Will return the results
# in a nicely formatted table.
#
###############################################################################

.include "./cs47_proj_macro.asm"
.include "./cs47_proj_procs.asm"

# data section
.data
askA: .asciiz "Please enter variable a: "
askB: .asciiz "Please enter variable b: "
header: .asciiz "Op\tType\t\tResult\tMatch?\n"
line: .asciiz "------------------------------------------\n"
norm: .asciiz "Normal\t\t"
logic: .asciiz "Logical\t\t"
multiplication: .asciiz "*\t"
division: .asciiz "/\t"
addition: .asciiz "+\t"
subtraction: .asciiz "-\t"
yes: .asciiz "YES\n"
no: .asciiz "NO\n"
tab: .asciiz "\t"
ellipsis: .asciiz "..."
pipe: .asciiz " | "
andStr: .asciiz " and "
nl: .asciiz "\n"
sp: .asciiz " "
.align 2
var_a: .space 4
var_b: .space 4
res_1: .space 4
res_2: .space 4
.text
.globl main
main:
	# Getting variables
	print_str(askA)
	li $v0, 5
	syscall
	sw $v0, var_a
	
	print_str(askB)
	li $v0, 5
	syscall
	sw $v0, var_b
	
	print_str(header)
	print_str(line)

add_:
	# Test normal addition
	lw $a0, var_a
	lw $a1, var_b
	li $a2, '+'
	jal au_normal
	move $t1, $v0
	sw $t1, res_1
	print_str(addition)
	print_str(norm)
	print_reg_int($t1)
	print_str(tab)
	print_str(ellipsis)
	print_str(nl)
	
	# Test logical addition
	lw $a0, var_a
	lw $a1, var_b
	li $a2, '+'
	lw $s0, var_a
	lw $s1, var_b
	jal au_logical
	move $t1, $v0
	print_str(addition)
	print_str(logic)
	print_reg_int($t1)
	print_str(tab)
	lw $t2, res_1
	bne $t1, $t2, fail_add
	print_str(yes)
	j sub_
fail_add:
	print_str(no)

sub_:
	# Test normal subtraction
	lw $a0, var_a
	lw $a1, var_b
	li $a2, '-'
	jal au_normal
	move $t1, $v0
	sw $t1, res_1
	print_str(subtraction)
	print_str(norm)
	print_reg_int($t1)
	print_str(tab)
	print_str(ellipsis)
	print_str(nl)
	
	# Test logical sub
	lw $a0, var_a
	lw $a1, var_b
	li $a2, '-'
	lw $s0, var_a
	lw $s1, var_b
	jal au_logical
	move $t1, $v0
	print_str(subtraction)
	print_str(logic)
	print_reg_int($t1)
	print_str(tab)
	lw $t2, res_1
	bne $t1, $t2, fail_sub
	print_str(yes)
	j mul_
fail_sub:
	print_str(no)

mul_:
	# Test normal multiplication
	lw $a0, var_a
	lw $a1, var_b
	li $a2, '*'
	jal au_normal
	move $t1, $v0
	sw $t1, res_1
	print_str(multiplication)
	print_str(norm)
	print_reg_int($t1)
	print_str(tab)
	print_str(ellipsis)
	print_str(nl)
	
	# Test logical multiplication
	lw $a0, var_a
	lw $a1, var_b
	li $a2, '*'
	jal au_logical
	move $t1, $v0
	print_str(multiplication)
	print_str(logic)
	print_reg_int($t1)
	print_str(tab)
	lw $t2, res_1
	bne $t1, $t2, fail_mul
	print_str(yes)
	j div_
fail_mul:
	print_str(no)

div_:
	# Test normal division
	lw $a0, var_a
	lw $a1, var_b
	li $a2, '/'
	jal au_normal
	move $t1, $v0
	move $t2, $v1
	sw $t1, res_1
	sw $t2, res_2
	print_str(division)
	print_str(norm)
	print_reg_int($t1)
	print_str(pipe)
	print_reg_int($t2)
	print_str(tab)
	print_str(ellipsis)
	print_str(nl)
	
	# Test logical division
	lw $a0, var_a
	lw $a1, var_b
	li $a2, '/'
	jal au_logical
	move $t1, $v0
	move $t2, $v1
	print_str(division)
	print_str(logic)
	print_reg_int($t1)
	print_str(pipe)
	print_reg_int($t2)
	print_str(tab)
	lw $t3, res_1
	lw $t4, res_2
	bne $t1, $t3, fail_div
	bne $t2, $t4, fail_div
	print_str(yes)
	exit
fail_div:
	print_str(no)
	exit
